package webdriverdto

import (
	"github.com/tebeka/selenium"
	"uni-crawl-frame/service/browsermobservice"
)

type CrawlWebDriverCtx struct {
	Service *selenium.Service
	XServer *browsermobservice.Server
	XClient *browsermobservice.Client
	Wd      selenium.WebDriver
	*CrawlBrowserDto
	*CrawlBrowserInfoDto
}

type CrawlBrowserDto struct {
	BrowserType string
	UserDataDir string
	ProxyPath   string
	Headless    bool

	BrowserInfos []*CrawlBrowserInfoDto
}

type CrawlBrowserInfoDto struct {
	DriverType   string
	DriverPath   string
	ExecutorPath string
	ProfilePath  string
	UriPrefix    string
}
