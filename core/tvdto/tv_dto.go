package tvdto

import "uni-crawl-frame/db/mysql/model/entity"

type CrawlTVCtx struct {
	VodTV     *entity.CmsCrawlVodTv
	VodTVItem *entity.CmsCrawlVodTvItem
}
