package core

import (
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/service/crawl/vodservice"
)

var StrategySelector strategySelector // 策略变量

type strategySelector interface {
	GetCrawlVodFlowStrategy(seed *entity.CmsCrawlQueue) CrawlVodFlowInterface  // 抓点播策略
	GetCrawlVodTVStrategy(seed *entity.CmsCrawlVodConfig) CrawlVodTVInterface  // 抓直播策略
	GetCrawlVodPadInfoStrategy(seed *entity.CmsCrawlVodTv) CrawlVodTVInterface // 填充点播业务信息策略
	GetHostType(crawlSeedUrl string) int                                       // todo 非策略方法应该移到其它地方
}

type DefaultStrategySelector struct {
}

func (r *DefaultStrategySelector) GetCrawlVodFlowStrategy(seed *entity.CmsCrawlQueue) CrawlVodFlowInterface {
	return nil
}

func (r *DefaultStrategySelector) GetCrawlVodTVStrategy(seed *entity.CmsCrawlVodConfig) CrawlVodTVInterface {
	return nil
}

func (r *DefaultStrategySelector) GetCrawlVodPadInfoStrategy(seed *entity.CmsCrawlVodTv) CrawlVodTVInterface {
	return nil
}

func (r *DefaultStrategySelector) GetHostType(crawlSeedUrl string) int {
	return vodservice.HostTypeNormal
}
