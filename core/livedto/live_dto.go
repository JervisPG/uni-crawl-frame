package livedto

import (
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/gogf/gf/v2/util/gconv"
	"time"
	"uni-crawl-frame/db/mysql/model/entity"
)

const (
	defaultWaitSeconds = 3
)

type CrawlLiveCtx struct {
	*entity.CmsCrawlLiveConfig
	NextLoadTime *gtime.Time
}

func (r *CrawlLiveCtx) LoadNextTimeDefault() {
	r.NextLoadTime = gtime.Now().Add(time.Second * time.Duration(defaultWaitSeconds))
}

func (r *CrawlLiveCtx) LoadNextTimeAfterSeconds(floatSeconds float32) {
	// 提前一些时间，一定程度防止丢帧
	r.NextLoadTime = gtime.Now().Add(time.Second * time.Duration(gconv.Int(floatSeconds*2/3)))
}
