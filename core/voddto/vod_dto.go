package voddto

import "uni-crawl-frame/db/mysql/model/entity"

type CrawlVodCtx struct {
	*entity.CmsCrawlVodConfig
	*entity.CmsCrawlVodConfigTask
}
