package boot

import (
	"github.com/gogf/gf/contrib/config/nacos/v2"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	"uni-crawl-frame/service/configservice"
)

func init() {
	var (
		ctx          = gctx.GetInitCtx()
		serverConfig = constant.ServerConfig{
			IpAddr: configservice.GetString("nacos.ipAddr", "localhost"),
			Port:   configservice.GetUint64("nacos.port", 8848),
		}
		clientConfig = constant.ClientConfig{
			CacheDir:    configservice.GetString("nacos.cacheDir", "./nacos/cache"),
			LogDir:      configservice.GetString("nacos.logDir", "./nacos/log"),
			NamespaceId: configservice.GetString("nacos.namespaceId", "uni_crawl"),
		}
		configParam = vo.ConfigParam{
			DataId: configservice.GetString("nacos.dataId", "config.yaml"),
			Group:  configservice.GetString("nacos.group", "uni_crawl"),
		}
	)
	// Create anacosClient that implements gcfg.Adapter.
	adapter, err := nacos.New(ctx, nacos.Config{
		ServerConfigs: []constant.ServerConfig{serverConfig},
		ClientConfig:  clientConfig,
		ConfigParam:   configParam,
	})
	if err != nil {
		g.Log().Fatalf(ctx, `%+v`, err)
	}
	// Change the adapter of default configuration instance.
	g.Cfg().SetAdapter(adapter)
}
