package replay

import (
	"context"
	"github.com/xxl-job/xxl-job-executor-go"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/service/crawl/replayservice"
)

var Manifest = new(CrawlReplayManifest)

type CrawlReplayManifest struct {
	programTask *entity.CmsCrawlReplayProgramTask
}

func (receiver *CrawlReplayManifest) InitManifestTasks(context context.Context, param *xxl.RunReq) (msg string) {
	replayservice.InitManifestTasks()
	return
}

func (receiver *CrawlReplayManifest) CrawlReplayManifest(context context.Context, param *xxl.RunReq) (msg string) {

	task := replayservice.GetManifestTask(replayservice.ManifestTaskInit)
	if task == nil {
		return
	}

	replayservice.UpdateManifestTaskStatus(task, replayservice.ManifestTaskCrawling)
	replayConfig := replayservice.GetReplayConfig(task.ReplayConfigId)
	receiver.doCrawlReplayManifest(replayConfig, task)
	replayservice.UpdateManifestTaskStatus(task, replayservice.ManifestTaskCrawlFinish)

	return
}

func (receiver *CrawlReplayManifest) doCrawlReplayManifest(replayConfig *entity.CmsCrawlReplayConfig, task *entity.CmsCrawlReplayManifestTask) {
	strategy := getCrawlReplayStrategy(replayConfig)
	strategy.CreateProgram(replayConfig, task)
}
