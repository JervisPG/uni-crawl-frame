package crawltask

import (
	"context"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/tebeka/selenium"
	"github.com/xxl-job/xxl-job-executor-go"
	"uni-crawl-frame/core"
	"uni-crawl-frame/core/voddto"
	"uni-crawl-frame/db/mysql/dao"
	"uni-crawl-frame/db/mysql/model/entity"
	"uni-crawl-frame/service/browsermobservice"
	"uni-crawl-frame/service/crawl/sysservice"
	"uni-crawl-frame/service/crawl/vodservice"
	"uni-crawl-frame/service/lockservice"
	"uni-crawl-frame/utils/browsermob"
	"uni-crawl-frame/utils/browserutil"
)

var CrawlVodTVTask = new(crawlVodTVTask)

type crawlVodTVTask struct {
}

func (crawlUrl *crawlVodTVTask) GenVodConfigTask(context context.Context, param *xxl.RunReq) (msg string) {
	vodConfig := vodservice.GetVodConfig()
	if vodConfig == nil {
		return
	}
	vodservice.UpdateVodConfig(vodConfig)

	configTask := new(entity.CmsCrawlVodConfigTask)
	configTask.VodConfigId = vodConfig.Id
	configTask.TaskStatus = vodservice.ConfigTaskStatusInit
	configTask.CreateTime = gtime.Now()

	dao.CmsCrawlVodConfigTask.Ctx(gctx.GetInitCtx()).Insert(configTask)

	return
}

func (crawlUrl *crawlVodTVTask) VodTVTask(context context.Context, param *xxl.RunReq) (msg string) {
	locked := lockservice.TryLockSeleniumLong()

	if !locked {
		return
	}
	defer lockservice.ReleaseLockSelenium()
	vodConfigTaskDO := vodservice.GetVodConfigTaskDO()
	if vodConfigTaskDO != nil {
		vodservice.UpdateVodConfigTaskStatus(vodConfigTaskDO.CmsCrawlVodConfigTask, vodservice.ConfigTaskStatusProcessing)
		DoStartCrawlVodTV(vodConfigTaskDO)
		vodservice.UpdateVodConfigTaskStatus(vodConfigTaskDO.CmsCrawlVodConfigTask, vodservice.ConfigTaskStatusOk)
	}

	return
}

// 填充视频基础信息
func (crawlUrl *crawlVodTVTask) VodTVPadInfoTask(context context.Context, param *xxl.RunReq) (msg string) {
	log := g.Log().Line()
	locked := lockservice.TryLockSelenium()
	if !locked {
		return
	}
	defer lockservice.ReleaseLockSelenium()

	vodTv := vodservice.GetVodTvByStatus(vodservice.CrawlTVInit)
	if vodTv == nil {
		return
	}

	log.Infof(gctx.GetInitCtx(),
		"更新vod tv. id = %v, to status = %v", vodTv.Id, vodservice.CrawlTVPadInfo)
	vodservice.UpdateVodTVStatus(vodTv, vodservice.CrawlTVPadInfo)

	DoStartCrawlVodPadInfo(vodTv)

	return
}

func DoStartCrawlVodTV(configTaskDO *voddto.CrawlVodCtx) {
	ctx := core.GetInitedCrawlContext()
	ctx.CrawlVodCtx = configTaskDO
	strategy := core.StrategySelector.GetCrawlVodTVStrategy(ctx.CrawlVodCtx.CmsCrawlVodConfig)
	ctx.CrawlByBrowserInterface = strategy

	if strategy.UseBrowser() {
		//g.Dump("使用浏览器")
		service, _ := browserutil.GetDriverService(browserutil.DriverServicePort)
		ctx.Service = service
		defer ctx.Service.Stop()

		proxyUrl := ""
		if strategy.UseCrawlerProxy() {
			proxyUrl = sysservice.GetRandomProxyUrl()
			ctx.Log.Infof(gctx.GetInitCtx(), "visit list page via proxy. domain = %v, proxy = %v", configTaskDO.DomainKeyPart, proxyUrl)
		}

		var caps selenium.Capabilities

		if strategy.UseBrowserMobProxy() {
			xServer := browsermobservice.NewServer(ctx.ProxyPath)
			xServer.Start()
			ctx.XServer = xServer
			defer ctx.XServer.Stop()
			proxy := xServer.CreateProxy(nil)
			ctx.XClient = proxy
			defer ctx.XClient.Close()
			caps = browserutil.GetAllCaps(ctx)
		} else {
			caps = browserutil.GetAllCapsChooseProxy(ctx, proxyUrl)
		}

		webDriver, err := browserutil.NewRemote(caps, browserutil.DriverServicePort, ctx.UriPrefix)
		ctx.Wd = webDriver
		if ctx.Wd == nil {
			ctx.CrawlVodCtx.CmsCrawlVodConfig.ErrorMsg = "WebDriver Init Fail"
			ctx.Log.Error(gctx.GetInitCtx(), err)
			return
		}
		defer ctx.Wd.Quit()

		// 业务处理-start
		if ctx.CrawlVodCtx.CmsCrawlVodConfig.SeedParams != "" {
			json, _ := gjson.LoadJson(ctx.CrawlVodCtx.CmsCrawlVodConfig.SeedParams)
			strategy.OpenBrowserWithParams(ctx, json)
		} else {
			if strategy.UseBrowserMobProxy() {
				browsermob.NewHarWait(ctx.Wd, ctx.XClient)
			}
			//g.Dump("打开浏览器")
			strategy.OpenBrowser(ctx)
		}
		// 业务处理-end
	}

	// 把URL,Headers信息保存起来
	strategy.FillTargetRequest(ctx)
}

func DoStartCrawlVodPadInfo(vodTVItem *entity.CmsCrawlVodTv) {
	ctx := core.GetInitedCrawlContext()
	ctx.CrawlTVCtx.VodTV = vodTVItem
	strategy := core.StrategySelector.GetCrawlVodPadInfoStrategy(ctx.CrawlTVCtx.VodTV)
	ctx.CrawlByBrowserInterface = strategy

	if strategy.UseBrowser() {
		//g.Dump("使用浏览器")
		service, _ := browserutil.GetDriverService(browserutil.DriverServicePort)
		ctx.Service = service
		defer ctx.Service.Stop()

		proxyUrl := ""
		if strategy.UseCrawlerProxy() {
			proxyUrl = sysservice.GetRandomProxyUrl()
			ctx.Log.Infof(gctx.GetInitCtx(),
				"visit detail page via proxy. id = %v, proxy = %v", vodTVItem.Id, proxyUrl)
		}

		var caps selenium.Capabilities

		if strategy.UseBrowserMobProxy() {
			xServer := browsermobservice.NewServer(ctx.ProxyPath)
			xServer.Start()
			ctx.XServer = xServer
			defer ctx.XServer.Stop()
			proxy := xServer.CreateProxy(nil)
			ctx.XClient = proxy
			defer ctx.XClient.Close()

			// BrowserMobProxy抓包方式
			caps = browserutil.GetAllCaps(ctx)
		} else {
			caps = browserutil.GetAllCapsChooseProxy(ctx, proxyUrl)
		}

		webDriver, err := browserutil.NewRemote(caps, browserutil.DriverServicePort, ctx.UriPrefix)
		ctx.Wd = webDriver
		if ctx.Wd == nil {
			ctx.CrawlTVCtx.VodTVItem.ErrorMsg = "WebDriver Init Fail"
			ctx.Log.Error(gctx.GetInitCtx(), err)
			return
		}
		defer ctx.Wd.Quit()

		// 业务处理-start
		if strategy.UseBrowserMobProxy() {
			browsermob.NewHarWait(ctx.Wd, ctx.XClient)
		}
		strategy.OpenBrowser(ctx)
		// 业务处理-end
	}

	// 把URL,Headers信息保存起来
	strategy.FillTargetRequest(ctx)
}
