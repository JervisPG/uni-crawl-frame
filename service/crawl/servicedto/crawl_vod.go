package servicedto

import "uni-crawl-frame/db/mysql/model/entity"

type CrawlVodConfigDO struct {
	*entity.CmsCrawlVodConfig
	*entity.CmsCrawlVodConfigTask
}
