package sysservice

import (
	"github.com/gogf/gf/v2/os/gctx"
	"uni-crawl-frame/db/mysql/dao"
	"uni-crawl-frame/db/mysql/model/entity"
)

var (
	sd = dao.SysDict.Columns()
)

func ListAllByTypeCode(typeCode string) []*entity.SysDict {
	var all []*entity.SysDict
	_ = dao.SysDict.Ctx(gctx.GetInitCtx()).
		Where(sd.TypeCode, typeCode).
		Where(sd.Status, 1).
		Scan(&all)
	return all
}
