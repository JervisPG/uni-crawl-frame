package configservice

import (
	"fmt"
	"github.com/gogf/gf/v2/encoding/gjson"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/util/gconv"
	"uni-crawl-frame/core/webdriverdto"
	"uni-crawl-frame/utils/constant"
)

func GetCrawlHostIp() string {
	return GetCrawlCfg("hostIp")
}

func GetCrawlCfg(key string) string {
	return GetString(fmt.Sprintf("crawl.%s", key))
}

func GetCrawlBool(key string) bool {
	return GetBool(fmt.Sprintf("crawl.%s", key))
}

func GetCrawlDebugBool(key string) bool {
	return GetBool(fmt.Sprintf("crawl.debug.%s", key))
}

func GetCrawlBrowser() *webdriverdto.CrawlBrowserDto {
	browserDO := new(webdriverdto.CrawlBrowserDto)
	browserDO.BrowserType = GetCrawlCfg(constant.CrawlBrowserType)
	browserDO.UserDataDir = GetCrawlCfg(constant.CrawlBrowserUserDataDir)
	browserDO.ProxyPath = GetCrawlCfg(constant.CrawlBrowserProxyPath)
	browserDO.Headless = GetCrawlBool(constant.CrawlBrowserHeadless)

	browserInfoList := GetArray("crawl.browser.browserInfoList")
	for _, item := range browserInfoList {
		infoDO := new(webdriverdto.CrawlBrowserInfoDto)
		infoJson := gjson.New(item)

		infoDO.DriverType = infoJson.Get("driverType").String()
		infoDO.DriverPath = infoJson.Get("driverPath").String()
		infoDO.ExecutorPath = infoJson.Get("executorPath").String()
		infoDO.ProfilePath = infoJson.Get("profilePath").String()

		if constant.DriverTypeChrome == infoDO.DriverType {
			infoDO.UriPrefix = "/wd/hub"
		}

		browserDO.BrowserInfos = append(browserDO.BrowserInfos, infoDO)
	}

	return browserDO
}

func GetCrawlBrowserInfo() *webdriverdto.CrawlBrowserInfoDto {
	browser := GetCrawlBrowser()
	if browser.BrowserInfos == nil {
		return nil
	}

	for _, info := range browser.BrowserInfos {
		if info.DriverType == browser.BrowserType {
			return info
		}
	}
	return nil
}

func GetCrawl(key string) string {
	return GetString(fmt.Sprintf("server.%s", key))
}

func GetServerCfg(key string) string {
	return GetString(fmt.Sprintf("server.%s", key))
}

func GetBool(key string) bool {
	value, err := g.Cfg().Get(gctx.GetInitCtx(), key)
	if err != nil {
		return false
	}
	return value.Bool()
}

func GetArray(key string) []interface{} {
	arr, err := g.Cfg().Get(gctx.GetInitCtx(), key)
	if err != nil {
		return nil
	}
	return arr.Array()
}

func GetStrings(key string) []string {
	var strs []string

	for _, item := range GetArray(key) {
		strs = append(strs, gconv.String(item))
	}
	return strs
}

func GetInt(key string, defValue int) int {
	value, _ := g.Cfg().Get(gctx.GetInitCtx(), key, defValue)
	return value.Int()
}

func GetString(key string, def ...interface{}) string {
	value, _ := g.Cfg().Get(gctx.GetInitCtx(), key, def)
	return value.String()
}

func GetUint64(key string, defValue uint64) uint64 {
	value, _ := g.Cfg().Get(gctx.GetInitCtx(), key, defValue)
	return value.Uint64()
}
